package com.decathlon.orderapi.valueobjects;

public enum OrderStatusEnum {
	SHIPPED("Shipped"),PRPEPARED_FOR_SHIPPING("PreparedForShipping"),CANCELLED("Cancelled");
	
	private String value;
	
	OrderStatusEnum(String value) {
		this.value = value;
	}
	
	public String getValue() {
		return this.value;
	}

}
