package com.decathlon.orderapi.exception;

public class OrderNotFoundException extends RuntimeException{
	private static final long serialVersionUID = -2807099736280586960L;

	public OrderNotFoundException(String message) {
		super(message);
	}
}
